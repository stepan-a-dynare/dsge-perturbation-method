addpath ../../matlab2tikz/src

MaxOrder = 5;

abscissa =  0:.01:2;
epsilon  = abscissa-1;

y = NaN(MaxOrder+1,length(epsilon));

y(1,:) = sqrt(abscissa);

for order=1:MaxOrder
    syms x;
    sqrt_approximation = taylor(sqrt(1+x),'Order',order+1);
    x = epsilon;
    y(order+1,:) = eval(sqrt_approximation);
end

base_color = [1,1,1];

hold on
for order=1:MaxOrder
    plot(abscissa,y(order+1,:),'Color',base_color-order/10); % Approximationn of the true function
end
plot(abscissa,y(1,:),'-k','linewidth',2); % True function
hold off

legend('First order','Second order','Third order','Fourth order','Fifth order','True','Location','SouthEast')
box on

set(gca,'XTick',[.2:.2:2])
set(gca,'XTickLabel',{'0.2'; '0.4'; '0.6'; '0.8'; '1.0'; '1.2'; '1.4'; '1.6'; '1.8'; '2.0'});

set(gca,'YTick',[.2:.2:1.4])
set(gca,'YTickLabel',{'0.2'; '0.4'; '0.6'; '0.8'; '1.0'; '1.2'; '1.4'});

matlab2tikz('../tex/sqrt_approximation_1.tikz', 'height', '\figureheight', 'width', '\figurewidth');

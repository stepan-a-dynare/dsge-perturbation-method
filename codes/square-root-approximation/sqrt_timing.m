number_of_iterations = 1e8;

a = 1.010;
e = a-1.0;

% Compute true value of sqrt(a)
tic
for i=1:number_of_iterations
    x = sqrt(a);
end
t0 = toc;

% First order approximation of sqrt(a)
tic
for i=1:number_of_iterations
    x1 = 1 + .5*e;
end
t1 = toc;


% Second order approximation of sqrt(a)
tic
for i=1:number_of_iterations
   x2 = 1 + .5*e-.125*e*e;
end
t2 = toc;

% Same approximation with a trick (by factorizing e we economize one multiplication per iteration)
tic
for i=1:number_of_iterations
    x2 = 1 + (.5-.125*e)*e;
end
t2_ = toc;

% Third order approximation of sqrt(a)
tic
for i=1:number_of_iterations
   x3 = 1 + .5*e-.125*e*e + .0625*e*e*e;
end
t3 = toc;

% Same approximation with a trick (by factorizing e we economize three multiplications per iteration)
tic
for i=1:number_of_iterations
    x3 = 1 + (.5-(.125 - .0625*e)*e)*e;
end
t3_ = toc;


disp(' ')
disp(['Execution time for evaluating sqrt(' num2str(a) ') is ' num2str(t0)])
disp(['Execution time for evaluating the first order approximation of sqrt(' num2str(a) ') is ' num2str(t1) ' [gain is ' num2str(-(t1/t0-1)*100) '%]'])
disp(['Execution time for evaluating the second order approximation of sqrt(' num2str(a) ') is ' num2str(t2_) ' (' num2str(t2)  ' without Horner factorization)' ' [gain is ' num2str(-(t2_/t0-1)*100) '%]'])
disp(['Execution time for evaluating the third order approximation of sqrt(' num2str(a) ') is ' num2str(t3_) ' (' num2str(t3)  ' without Horner factorization)' ' [gain is ' num2str(-(t3_/t0-1)*100) '%]'])
disp(' ')
disp(['Approximation error of the first order approximation is ' num2str(x1-x)])
disp(['Approximation error of the second order approximation is ' num2str(x2-x)])
disp(['Approximation error of the third order approximation is ' num2str(x3-x)])
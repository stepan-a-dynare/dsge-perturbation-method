// First order perturbation simulation
@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = LoggedProductivityInnovation/(1-rho);
  Capital = (exp(LoggedProductivity)*alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = exp(LoggedProductivity)*Capital^alpha-delta*Capital;
end;

steady;

shocks;
    var LoggedProductivityInnovation = .01^2;
end;

stoch_simul(order=1,periods=1000);

figure('name','Policy rule');
plot(Capital,Consumption,'ok');
